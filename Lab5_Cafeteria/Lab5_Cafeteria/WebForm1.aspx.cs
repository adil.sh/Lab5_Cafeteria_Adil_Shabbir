﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab5_Cafeteria
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        // create a connection
       // SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["CafeteriaContext"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnlogin_Click(object sender, EventArgs e)
        {

            CafeteriaContext db = new CafeteriaContext();

            // client login
            var result = (from el in db.Clients
                              // orderby el.Nom descending
                          where el.Username == txtusername.Text && el.Password == txtpassword.Text
                          select new { el.Username, el.Password}
                         );
            

            try
            {
                string usr = result.First().Username;
                string pswd = result.First().Password;

                if ((txtusername.Text == usr && pswd == txtpassword.Text) && txtusername.Text != "" && txtpassword.Text!= "") { 
                    Label1.Text = "Succes";
                    Server.Transfer("proposerRecette.aspx");
                }

                if (txtusername.Text == "" && txtpassword.Text == "")
                { Label1.Text = "Login Failed! Please Check Username or Password"; }
             

            }
            catch (Exception)
            {

                Label1.Text = "Login Failed! Please Check Username or Password";
            }


            // client login end

            // admin login

            var resultAdmin = (from ad in db.admins
                              // orderby el.Nom descending
                          where ad.Username == txtusernameadmin.Text && ad.Password == txtpasswordadmin.Text
                          select new { ad.Username, ad.Password }
                        );


            try
            {
                string usrA = resultAdmin.First().Username;
                string pswdA = resultAdmin.First().Password;

                if ((txtusernameadmin.Text == usrA && pswdA == txtpasswordadmin.Text) && txtusernameadmin.Text != "" && txtpasswordadmin.Text != "")
                {
                    Label1.Text = "Succes";
                    Server.Transfer("admin.aspx");
                }

                if (txtusernameadmin.Text == "" && txtpasswordadmin.Text == "")
                { Label1.Text = "Login Failed! Please Check Username or Password"; }


            }
            catch (Exception)
            {

                Label1.Text = "Login Failed! Please Check Username or Password";
            }


            // admin login end




            //else
            //{
            //    Label1.Text = "LOGIN FAILED \n CREATE AN ACCOUNT OR RESET YOUR PASSWORD";
            //}

            //GridView1.DataSource = result;
            //GridView1.DataBind();



            /*SqlCommand comm = new SqlCommand();

            comm.CommandText = "select * from clients where Username=@Username and Password=@Password ";
            comm.Connection = conn;
            comm.Parameters.AddWithValue("@Username", txtusername.Text);
            comm.Parameters.AddWithValue("@Password", txtpassword.Text);

            //open connection
            conn.Open();

            SqlDataReader rd = comm.ExecuteReader();

            if (rd.HasRows)
                Response.Write("Success!");
            else
                Response.Write("Failed!");*/
        }
    }
}