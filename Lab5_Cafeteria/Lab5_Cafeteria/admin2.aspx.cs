﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab5_Cafeteria
{
    public partial class WebForm2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CafeteriaContext db = new CafeteriaContext();

            var result = (from el in db.Recettes
                          select new { el.Nom, el.Ingredients, el.Etapes, el.Date, el.ClientUsername }).ToList();
            GridView1.DataSource = result;
            GridView1.DataBind();
        }
    }
}