﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="Lab5_Cafeteria.index" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>HOME</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.aspx">Cafeteria Teccart</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="index.aspx">HOME</a></li>
      <li><a href="webForm1.aspx">LOGIN</a></li>
      <li><a href="register.aspx">REGISTER</a></li>
   
    </ul>
  </div>
</nav>
  
<div class="container text-center">
<!-- -->
    <form id="form1" runat="server">
    <div>
            <h1> Welcome to our Teccarts Cafeteria </h1>
        <br />
            <h2> Down below are our submitted receipes </h2>
            
            <br />
    </div>
        <div align="center">
        <asp:GridView ID="GridView1" runat="server" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" BackColor="White" BorderStyle="Solid" BorderWidth="3px" CellPadding="5" CellSpacing="5">
            <AlternatingRowStyle BackColor="#CCCCCC" BorderColor="Red" />
            <EditRowStyle BackColor="#66FFFF" />
        </asp:GridView>
            </div>
        <br />
        Click <a href ="webForm1.aspx"> here to login </a> to submit yours</form>
    </div>
</body>
</html>
