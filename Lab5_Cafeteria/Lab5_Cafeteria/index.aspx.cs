﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab5_Cafeteria
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CafeteriaContext db = new CafeteriaContext();

            var result = (from el in db.Recettes
                         select new { el.Nom, el.Ingredients, el.Etapes, el.Date, el.ClientUsername }).ToList();
            GridView1.DataSource = result;
            GridView1.DataBind();

            /*comment effectuer un select*/

            // var result = from el in db.Programme
            //             orderby el.DepartementId descending
            //             select new { el.Id, el.ProgrammeNom, el.ProgrammeCode, el.DepartementId };
            // GridView1.DataSource = result;
            // GridView1.DataBind();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}