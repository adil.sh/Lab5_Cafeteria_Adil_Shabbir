﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace Lab5_Cafeteria
{
    public partial class proposerRecette : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                Page lastpage = (Page)Context.Handler;

                Label1.Text = ((TextBox)lastpage.FindControl("txtusername")).Text;
                string usrname = Label1.Text;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = Label1.Text;

            //  CafeteriaContext db = new CafeteriaContext();

            /*  var results = (from c in db.Clients
                             join r in db.Recettes on c.Id equals r.ClientsId
                             where c.Username == Label2.Text
                             select new { c.Id}
                           );
                           */

            var context = new CafeteriaContext();

            Recette rc = new Recette();         //Make sure you have a table called test in DB

           
            {

                rc.Nom = TextBoxname.Text;
                rc.Ingredients = TextBoxIng.Text;
                rc.Etapes = TextBoxSteps.Text;
                rc.Type = TextBoxType.Text;
                rc.Date = TextBoxDate.Text;
                rc.Photo = TextBoxpic.Text;
                rc.ClientUsername = Label1.Text;
                // ClientsId = Convert.ToInt32(Label2.Text)
            };
            
            context.Recettes.Add(rc);
            try
            {
                context.SaveChanges();
                Label4.Text = "Receipie Added! Go to Home page to see all the receipes";
            }
            catch (DbEntityValidationException ex)
            {
                foreach (var entityValidationErrors in ex.EntityValidationErrors)
                {
                    foreach (var validationError in entityValidationErrors.ValidationErrors)
                    {
                        Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);
                    }
                }

                // ******************** following code was a test 

                //if (TextBoxnom.Text != string.Empty && TextBoxprenom.Text != string.Empty && TextBoxusr.Text != string.Empty && TextBoxpsw.Text != string.Empty)
                //{
                //    Label1.Text = "Registration Complete";
                //}
                //else
                //{
                //    Label6.Text = "Please make sure all fields are porperly completed";
                //}
                /*comment effectuer un join entre deux table*/

                //var result1 = from p in db.Programme
                //              join d in db.Departement on p.DepartementId equals d.Id
                //              select new
                //              {
                //                  prog = p.ProgrammeNom,
                //                  dep = d.DepartementNom
                //              };
            }
        }
    }
}