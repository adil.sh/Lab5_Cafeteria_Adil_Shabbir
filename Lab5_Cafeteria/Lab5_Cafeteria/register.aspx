﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="Lab5_Cafeteria.register" %>


<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
  <title>Register</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.aspx">Cafeteria Teccart</a>
    </div>
    <ul class="nav navbar-nav">
      <li class=""><a href="index.aspx">HOME</a></li>
      <li><a href="webForm1.aspx">LOGIN</a></li>
      <li><a href="register.aspx">REGISTER</a></li>
   
    </ul>
  </div>
</nav>
  
<div class="container text-center">


<!-- -->

   

    <form id="form1" runat="server">

    <div>
         <div align="center">
        <h1> Register here</h1>
        <asp:Label ID="Label6" runat="server" ForeColor="Red"></asp:Label>
        <br />
        <br />

        <table cellpadding="2" class="auto-style1">
            <tr>
                <td class="auto-style4">Name</td>
                <td class="auto-style5">
                    <asp:TextBox ID="TextBoxnom" runat="server" Height="41px" Width="264px"></asp:TextBox>
                </td>
                <td class="auto-style6">
                    <asp:Label ID="Label2" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
              <tr>
                <td class="auto-style2">Prenom</td>
                <td class="auto-style3">
                    <br />
                    <asp:TextBox ID="TextBoxprenom" runat="server" Height="43px" Width="264px"></asp:TextBox>
                  </td>
                <td>
                    <asp:Label ID="Label3" runat="server" ForeColor="Red"></asp:Label>
                  </td>
            </tr>
            <tr>
                <td class="auto-style2">Username</td>
                <td class="auto-style3">
                    <br />
                    <asp:TextBox ID="TextBoxusr" runat="server" Height="42px" Width="264px"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label4" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Password</td>
                <td class="auto-style3">
                    <br />
                    <asp:TextBox ID="TextBoxpsw" runat="server" Height="40px" TextMode="Password" Width="264px"></asp:TextBox>
                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style3">
                    <asp:Button ID="Button1" runat="server" Height="41px" OnClick="Button1_Click" Text="Register" Width="168px" />
                </td>
                <td>
                    <asp:Label ID="Label1" runat="server" ForeColor="#33CC33"></asp:Label>
                    <br />
                    <p> Registration Complete? <a href="Webform1.aspx"> Click here to Login </a></p>
                </td>
            </tr>
        </table>
        
    </div>
    </form>
        </div>
    </div>
    
</body>
</html>
        <style type="text/css">
        .auto-style1 {
            width: 62%;
        }
        .auto-style2 {
            width: 137px;
        }
        .auto-style3 {
            width: 292px;
        }
        .auto-style4 {
            width: 137px;
            height: 44px;
        }
        .auto-style5 {
            width: 292px;
            height: 44px;
        }
        .auto-style6 {
            height: 44px;
        }
    </style>